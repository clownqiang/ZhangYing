package com.clownqiang.zhangyingtest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import com.clownqiang.zhangyingtest.option_state.ColorState;
import com.clownqiang.zhangyingtest.option_state.LightState;
import com.clownqiang.zhangyingtest.option_state.SaturationState;
import com.clownqiang.zhangyingtest.option_state.StateManager;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements
        SeekBar.OnSeekBarChangeListener, RadioGroup.OnCheckedChangeListener {

    StateManager stateManager;
    LightState lightState;
    ColorState colorState;
    SaturationState saturationState;

    @Bind(R.id.tv_light)
    TextView lightView;
    @Bind(R.id.tv_color)
    TextView colorView;
    @Bind(R.id.tv_saturation)
    TextView saturationView;
    @Bind(R.id.sb)
    SeekBar seekBar;
    @Bind(R.id.rg)
    RadioGroup radioGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setListeners();
        initStates();
    }

    private void setListeners() {
        seekBar.setOnSeekBarChangeListener(this);
        radioGroup.setOnCheckedChangeListener(this);
    }

    private void initStates() {
        /**
         * 绑定状态Color，Light，Saturation，切换状态
         * */
        stateManager = new StateManager();
        lightState = new LightState(lightView);
        colorState = new ColorState(colorView);
        saturationState = new SaturationState(saturationView);
        stateManager.getStates().put(R.id.rb_light, lightState);
        stateManager.getStates().put(R.id.rb_color, colorState);
        stateManager.getStates().put(R.id.rb_saturation, saturationState);
        stateManager.changeState(R.id.rb_light);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int value, boolean b) {
        stateManager.valueChange(value);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int checkId) {
        stateManager.changeState(checkId);
    }

    /**
     * 获取TextView
     * TextView getLightView()
     * TextView getColorView()
     * TextView getSaturation()
     * */
}
