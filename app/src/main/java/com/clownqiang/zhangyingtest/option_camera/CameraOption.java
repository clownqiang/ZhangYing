package com.clownqiang.zhangyingtest.option_camera;

/**
 * Created by clownqiang on 16/3/28.
 */
public interface CameraOption {

    /**
     * 打开相机
     */
    public void openCamera();

    /**
     * 关闭相机
     */
    public void closeCamera();

}
