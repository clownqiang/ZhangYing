package com.clownqiang.zhangyingtest.option_state;

import android.widget.TextView;

/**
 * Created by clownqiang on 16/3/28.
 */
public class ColorState implements State {

    TextView colorView;

    public ColorState(TextView colorView) {
        this.colorView = colorView;
    }

    @Override
    public void valueChanged(int value) {
        colorView.setText("色彩："+String.valueOf(value));
    }
}
