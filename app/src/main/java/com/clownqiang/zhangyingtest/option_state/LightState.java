package com.clownqiang.zhangyingtest.option_state;

import android.widget.TextView;

/**
 * Created by clownqiang on 16/3/28.
 */
public class LightState implements State {

    TextView lightView;

    public LightState(TextView lightView) {
        this.lightView = lightView;
    }

    @Override
    public void valueChanged(int value) {
        lightView.setText("亮度："+String.valueOf(value));
    }
}
