package com.clownqiang.zhangyingtest.option_state;

import android.widget.TextView;

/**
 * Created by clownqiang on 16/3/28.
 */
public class SaturationState implements State {

    TextView saturationState;

    public SaturationState(TextView saturationState) {
        this.saturationState = saturationState;
    }

    @Override
    public void valueChanged(int value) {
        saturationState.setText("饱和度：" + String.valueOf(value));
    }
}
