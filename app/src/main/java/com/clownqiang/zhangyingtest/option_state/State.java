package com.clownqiang.zhangyingtest.option_state;

/**
 * Created by clownqiang on 16/3/28.
 */
public interface State {
    void valueChanged(int value);
}
