package com.clownqiang.zhangyingtest.option_state;

import java.util.HashMap;

/**
 * Created by clownqiang on 16/3/28.
 */
public class StateManager {
    HashMap<Integer, State> states = new HashMap<>();
    State currentState;


    public HashMap<Integer, State> getStates() {
        return states;
    }

    public void changeState(int checkedId) {
        currentState = states.get(checkedId);
    }

    public void valueChange(int value) {
        currentState.valueChanged(value);
    }
}
