package com.clownqiang.zhangyingtest.view;

import android.widget.TextView;

/**
 * Created by clownqiang on 16/3/29.
 */
public interface IMainView extends IBaseView {

    public TextView getColorView();

    public TextView getLightView();

    public TextView getSaturationView();
}
